'use strict';

/**
2d-space (x,y), grid is given by Array[y][x]
Origin at top-left corner, x - horizontal axis, y - vertical axis
Object sizes are non-negative
*/

//Class Vector
	
	function Vector(x = 0,y = 0){
		this.x = x;	
		this.y = y;
	}

	Vector.prototype.plus = function(vector){
		if (!isVector(vector)){
			throw new Error("Vector.prototype.plus: argument is not a Vector");
		};
		return new Vector(this.x+vector.x, this.y+vector.y);
	}

	Vector.prototype.times = function(scale){
		if (isNaN(scale)){
			throw new Error("Множитель не является числом");
		}
		return new Vector(this.x*scale, this.y*scale);
	}
	
//Class Actor

	function Actor(pos = new Vector(), size = new Vector(1,1), speed = new Vector()){
		if (!(isVector(pos) 
				&& isVector(size) 
				&& isVector(speed)))
		{
			throw new Error("Actor: argument is not actor");
		}
		this.pos = pos;
		this.size = size;
		this.speed = speed;
	}

	Object.defineProperty(Actor.prototype, 'left', 	{ get: function () {return this.pos.x}});
	Object.defineProperty(Actor.prototype, 'top', 	{ get: function () {return this.pos.y}});
	Object.defineProperty(Actor.prototype, 'right', { get: function () {return this.pos.x + this.size.x}});
	Object.defineProperty(Actor.prototype, 'bottom',{ get: function () {return this.pos.y + this.size.y}});
	Object.defineProperty(Actor.prototype, 'type', 	{ value: "actor",  writable: false });

	Actor.prototype.act = function(){};

	Actor.prototype.isIntersect = function(actor){
		
		if (!isActor(actor)){
			throw new Error("Actor.prototype.isIntersect: argument is not Actor");
		};
			
		if (actor == this ||
			this.right <= actor.left ||
			this.left >= actor.right ||	
			this.top >= actor.bottom ||
			this.bottom <= actor.top)
		{
			return false;
		}
		return true;
	};

//Class Level

	function Level(grid = undefined, actors = undefined){
		if (actors !== undefined){
			this.player = actors.find((element) => element.type == 'player');
		};
		this.finishDelay = 1;
		this.status = null;
		
		this.grid = grid;
		this.actors = actors;
		this.height = getHeight(grid);
		this.width = getWidth(grid);
	}

	Level.prototype.isFinished = function(){
			return (this.status === null || this.finishDelay > 0) ? false : true;
	}

	Level.prototype.actorAt = function(obj){
		if (!isActor(obj) || obj === undefined){
			throw new Error("Level.prototype.actorAt: argument is not Actor or undefined");
		}
		return (this.actors === undefined) ? undefined : this.actors.find((element) => element.isIntersect(obj));
	}

	Level.prototype.obstacleAt = function(position, size = new Vector(1,1)){
		if (!isVector(position) || !isVector(size)){
			throw new Error("Level.prototype.obstacleAt: argument is not a vector");
		}
		
		//object boundaries
		let left = position.x;
		let right = left + size.x;
		let top = position.y;
		let bottom = top + size.y;
		
		if (left < 0 || right > this.width || top < 0){
			return "wall";
		}

		if (bottom > this.height){
			return "lava";
		}
		
		//determine grid where obstacles potentially overlap object's area
		let minX = isInt(left) 	? left  - 1 : Math.floor(left);
		let maxX = isInt(right) ? right - 1 : Math.floor(right);
		let minY = isInt(top) 	? top   - 1 : Math.floor(top);
		let maxY = isInt(bottom)? bottom- 1 : Math.floor(bottom);

		let result = undefined;
		
		//search obstacles
		for (let x = minX; x <= maxX; x++){
			for (let y = minY; y <= maxY; y++){
				if (this.grid[y][x] !== undefined){
					result = this.grid[y][x];
					break;
				}
			}
			if (result !== undefined){
				break;
			}
		};
		return result;
	}
	
	Level.prototype.removeActor = function(actor){
		if (this.actors.includes(actor)){
			let index = this.actors.findIndex((element) => (actor === element));
			this.actors.splice(index,1);
		}
	}
		
	Level.prototype.noMoreActors = function(type){
		if (this.actors === undefined){
			return true;		
		};
		return (this.actors.findIndex(element => (element.type == type)) == -1);
	}
	
	const losingTypes = ["lava", "fireball"];
	const pickingTypes = ["coin", "mushroom", "berry"];

	Level.prototype.playerTouched = function(type, actor){
			
		if (this.status !== null){
			return;
		}		
		if (losingTypes.includes(type)){
			this.status = 'lost';
		}
		if (actor !== undefined && pickingTypes.includes(type)){
			this.removeActor(actor);
			if (pickingTypes.every((pickingType) => this.noMoreActors(pickingType))){
				this.status = "won";
			}
		}
	}

//Class LevelParser	
	
	function LevelParser(dictionary){
		this.dictionary = dictionary;	
	}

	LevelParser.prototype.actorFromSymbol = function(notation){
		if (notation === undefined){
			return undefined;
		}
		let key = Object.keys(this.dictionary).find((key) => (key === notation));
		return (key === undefined) ? undefined : this.dictionary[key];
	}
		
	LevelParser.prototype.obstacleFromSymbol = function(notation){
		switch (notation){
			case "x"		: return "wall";
			case "!" 		: return "lava";
			default			: return undefined;
		}
	}
		
	LevelParser.prototype.createGrid = function(stringArray){
		var result = stringArray.map(column => column.split("").map(notation => this.obstacleFromSymbol(notation)));
		return result;
	}

	LevelParser.prototype.createActors = function(stringArray){
		var result =[];
		if (this.dictionary === undefined || stringArray === undefined){
			return result;
		};
			
		stringArray.forEach((row, indY) => {
			row.split("").forEach((notation, indX) => {
				if (this.actorFromSymbol(notation) !== undefined){
					let key = Object.keys(this.dictionary).find((key) => (key === notation));
					if (isActorConstructor(this.dictionary[key])) {
						result.push(new this.dictionary[key](new Vector(indX, indY)));
					}
				};
			});
		});
		return result;
	}
	
	LevelParser.prototype.parse = function(plan){
		let grid = this.createGrid(plan);
		let actors = this.createActors(plan);
		return new Level(grid, actors);
	}

//Class Fireball	
	
	function Fireball(position = new Vector(), speed = new Vector()){
		Actor.apply(this, [position, new Vector(1,1), speed]);
	}

	Fireball.prototype = Object.create(Actor.prototype);
	Object.defineProperty(Fireball.prototype, 'type', { value: "fireball",  writable: false });

	Fireball.prototype.getNextPosition = function(time = 1){
		return this.pos.plus(this.speed.times(time));
	}

	Fireball.prototype.handleObstacle = function(){
		this.speed = this.speed.times(-1);
	}

	Fireball.prototype.act = function(time, level){

	let isObstacle = level.obstacleAt(this.getNextPosition(time), this.size);
		if (!isObstacle){
				this.pos = this.getNextPosition(time);
		}else{
			this.handleObstacle();
		}
	}

//Classes HoizontalFireball, VerticalFireball, FireRain
	
	function HorizontalFireball(position){
		Fireball.apply(this,[position, new Vector(2,0)]);
	}

	HorizontalFireball.prototype = Object.create(Fireball.prototype);

	function VerticalFireball(position){
		Fireball.apply(this,[position, new Vector(0,2)]);
	}

	VerticalFireball.prototype = Object.create(Fireball.prototype);

	function FireRain(position){
		Fireball.apply(this,[position, new Vector(0,3)]);
		Object.defineProperty(this, 'defaultPos', { value: position,  writable: false });
	}

	FireRain.prototype = Object.create(Fireball.prototype);
	FireRain.prototype.handleObstacle = function(){
		this.pos = this.defaultPos;
	}

//Class Coin	
	
	function Coin(position){
		if (position === undefined){
			position = new Vector();
		}
		let shift = new Vector(0.2, 0.1);
		let coinPosition = position.plus(shift);
		let coinSize = new Vector(0.6,0.6);
		
		Actor.apply(this, [coinPosition, coinSize]);
		
		let springPhase = getRandom(0, 2*Math.PI);

		Object.defineProperty(this, 'spring', { value: springPhase, writable: true, configurable: false });
		Object.defineProperty(this, 'basePosition', { value: coinPosition, writable: false });	
	}

	Coin.prototype = Object.create(Actor.prototype);
	Object.defineProperty(Coin.prototype, 'type', 			{ value: "coin",  writable: false });  
	Object.defineProperty(Coin.prototype, 'springSpeed', 	{ value: 8,  writable: false });
	Object.defineProperty(Coin.prototype, 'springDist', 	{ value: 0.07,  writable: false });

	Coin.prototype.updateSpring = function(time = 1){
		this.spring += this.springSpeed*time;
	}

	Coin.prototype.getSpringVector = function(){
		return new Vector(0, Math.sin(this.spring)*this.springDist);
	}

	Coin.prototype.getNextPosition = function(time = 1){
		
		this.updateSpring(time);
		return this.basePosition.plus(this.getSpringVector().times(time));
	}

	Coin.prototype.act = function(time = 1){
		this.pos = this.getNextPosition(time);
	}
	
//Class Mushroom, Berry
	function Mushroom(position){
		Coin.apply(this,[position]);
	}

	Mushroom.prototype = Object.create(Coin.prototype);
	Object.defineProperty(Mushroom.prototype, 'type', 			{ value: "mushroom",  writable: false });  
	Object.defineProperty(Mushroom.prototype, 'springDist', 	{ value: 4,  writable: false });

	function Berry(position){
		Coin.apply(this,[position]);
	}

	Berry.prototype = Object.create(Coin.prototype);
	Object.defineProperty(Berry.prototype, 'type', 			{ value: "berry",  writable: false });  
	Object.defineProperty(Berry.prototype, 'springDist', 	{ value: 10,  writable: false });
	
//Class Player

	function Player(position = new Vector()){
		let shift = new Vector(0,-0.5);
		let size = new Vector(0.8, 1.5);
		Actor.apply(this, [position.plus(shift), size]);
	}

	Player.prototype = Object.create(Actor.prototype);
	Object.defineProperty(Player.prototype, 'type', { value: "player",  writable: false });

//Additional functions
	
	function getHeight(array){
		return (array === undefined) ? 0 : array.length;
	}

	function getWidth(array){
		return (array === undefined) ? 0 : array.reduce((currentMax, row) => Math.max(currentMax, row.length), 0);
	}

	function isInt(n){ 
		//positive integer
		return n%1 === 0 && n > 0;
	}

	function isVector(vector){
		return vector instanceof Vector;
	}

	function isActor(actor){
		return actor instanceof Actor;
	}	
	
	function isActorConstructor(object){
		try{
			return isActor(new object());
		}catch(err){
			return false;
		}
	}
	
	function getRandom(min, max) {
	  return Math.random() * (max - min) + min;
	}

//Initialize game

	const actorDict = {
	  '@': Player,
	  'v': FireRain,
	  'o': Coin,
	  'm': Mushroom,
	  'b': Berry,
	  '=': HorizontalFireball,
	  '|': VerticalFireball
	};

	const Parser = new LevelParser(actorDict);

	loadLevels()
	.then((plans)=> {
		runGame(JSON.parse(plans), Parser, DOMDisplay);
	})
	.catch((error)=> console.log(error));